SELECT name 
FROM artists
WHERE name LIKE "%D%";

SELECT *
FROM songs
WHERE length < 230;

SELECT albums.name AS album_name, songs.title AS song_title, songs.length AS song_length
FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT artists.name AS artist_name, albums.name AS album_name
FROM artists JOIN albums ON artists.id = albums.artist_id
WHERE albums.name LIKE "%A%";

SELECT *
FROM albums
ORDER BY name DESC
LIMIT 4;

SELECT albums.name AS album_name, songs.title AS song_title
FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY album_name DESC, song_title ASC;